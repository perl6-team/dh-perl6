#!/usr/bin/perl

use warnings;
use strict;

use Debian::Debhelper::Dh_Lib;

# dh_perl6 creates postinst/postrm scripts
insert_after("dh_install", "dh_perl6_maintscript");
insert_after("dh_install", "dh_perl6_depsfile");

# automatically run the test suite
insert_after("dh_auto_test", "dh_perl6_test");
remove_command("dh_auto_test");

1;
